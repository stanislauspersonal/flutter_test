import 'dart:math';
import 'dart:typed_data';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:path_provider/path_provider.dart';
import 'package:http/http.dart' as http;



Map<String, dynamic> sampleRecipeMap;
List<dynamic> sampleInstructMap;
Recipe sampleRecipe;
List<dynamic> recipes;

String json1 = '{ "id": 716429, "title": "Pasta with Garlic, Scallions, Cauliflower & Breadcrumbs", "image": "https://spoonacular.com/recipeImages/716429-556x370.jpg", "imageType": "jpg", "servings": 2, "readyInMinutes": 45, "license": "CC BY-SA 3.0", "sourceName": "Full Belly Sisters", "sourceUrl": "http://fullbellysisters.blogspot.com/2012/06/pasta-with-garlic-scallions-cauliflower.html", "spoonacularSourceUrl": "https://spoonacular.com/pasta-with-garlic-scallions-cauliflower-breadcrumbs-716429", "aggregateLikes": 209, "healthScore": 19.0, "spoonacularScore": 83.0, "pricePerServing": 163.15, "analyzedInstructions": [], "cheap": false, "creditsText": "Full Belly Sisters", "cuisines": ["French"], "dairyFree": false, "diets": [], "gaps": "no", "glutenFree": false, "instructions": "", "ketogenic": false, "lowFodmap": false, "occasions": [], "sustainable": false, "vegan": false, "vegetarian": false, "veryHealthy": false, "veryPopular": false, "whole30": false, "weightWatcherSmartPoints": 17, "dishTypes": [ "lunch", "main course", "main dish", "dinner" ], "extendedIngredients": [ { "aisle": "Milk, Eggs, Other Dairy", "amount": 1.0, "consitency": "solid", "id": 1001, "image": "butter-sliced.jpg", "measures": { "metric": { "amount": 1.0, "unitLong": "Tbsp", "unitShort": "Tbsp" }, "us": { "amount": 1.0, "unitLong": "Tbsp", "unitShort": "Tbsp" } }, "meta": [], "name": "butter", "original": "1 tbsp butter", "originalName": "butter", "unit": "tbsp" }, { "aisle": "Produce", "amount": 2.0, "consitency": "solid", "id": 10011135, "image": "cauliflower.jpg", "measures": { "metric": { "amount": 473.176, "unitLong": "milliliters", "unitShort": "ml" }, "us": { "amount": 2.0, "unitLong": "cups", "unitShort": "cups" } }, "meta": [ "frozen", "thawed", "cut into bite-sized pieces" ], "name": "cauliflower florets", "original": "about 2 cups frozen cauliflower florets, thawed, cut into bite-sized pieces", "originalName": "about frozen cauliflower florets, thawed, cut into bite-sized pieces", "unit": "cups" }, { "aisle": "Cheese", "amount": 2.0, "consitency": "solid", "id": 1041009, "image": "cheddar-cheese.png", "measures": { "metric": { "amount": 2.0, "unitLong": "Tbsps", "unitShort": "Tbsps" }, "us": { "amount": 2.0, "unitLong": "Tbsps", "unitShort": "Tbsps" } }, "meta": [ "grated", "(I used romano)" ], "name": "cheese", "original": "2 tbsp grated cheese (I used romano)", "originalName": "grated cheese (I used romano)", "unit": "tbsp" }, { "aisle": "Oil, Vinegar, Salad Dressing", "amount": 1.0, "consitency": "liquid", "id": 1034053, "image": "olive-oil.jpg", "measures": { "metric": { "amount": 1.0, "unitLong": "Tbsp", "unitShort": "Tbsp" }, "us": { "amount": 1.0, "unitLong": "Tbsp", "unitShort": "Tbsp" } }, "meta": [], "name": "extra virgin olive oil", "original": "1-2 tbsp extra virgin olive oil", "originalName": "extra virgin olive oil", "unit": "tbsp" }, { "aisle": "Produce", "amount": 5.0, "consitency": "solid", "id": 11215, "image": "garlic.jpg", "measures": { "metric": { "amount": 5.0, "unitLong": "cloves", "unitShort": "cloves" }, "us": { "amount": 5.0, "unitLong": "cloves", "unitShort": "cloves" } }, "meta": [], "name": "garlic", "original": "5-6 cloves garlic", "originalName": "garlic", "unit": "cloves" }, { "aisle": "Pasta and Rice", "amount": 6.0, "consitency": "solid", "id": 20420, "image": "fusilli.jpg", "measures": { "metric": { "amount": 170.097, "unitLong": "grams", "unitShort": "g" }, "us": { "amount": 6.0, "unitLong": "ounces", "unitShort": "oz" } }, "meta": [ "(I used linguine)" ], "name": "pasta", "original": "6-8 ounces pasta (I used linguine)", "originalName": "pasta (I used linguine)", "unit": "ounces" }, { "aisle": "Spices and Seasonings", "amount": 2.0, "consitency": "solid", "id": 1032009, "image": "red-pepper-flakes.jpg", "measures": { "metric": { "amount": 2.0, "unitLong": "pinches", "unitShort": "pinches" }, "us": { "amount": 2.0, "unitLong": "pinches", "unitShort": "pinches" } }, "meta": [ "red" ], "name": "red pepper flakes", "original": "couple of pinches red pepper flakes, optional", "originalName": "couple of red pepper flakes, optional", "unit": "pinches" }, { "aisle": "Spices and Seasonings", "amount": 2.0, "consitency": "solid", "id": 1102047, "image": "salt-and-pepper.jpg", "measures": { "metric": { "amount": 2.0, "unitLong": "servings", "unitShort": "servings" }, "us": { "amount": 2.0, "unitLong": "servings", "unitShort": "servings" } }, "meta": [ "to taste" ], "name": "salt and pepper", "original": "salt and pepper, to taste", "originalName": "salt and pepper, to taste", "unit": "servings" }, { "aisle": "Produce", "amount": 3.0, "consitency": "solid", "id": 11291, "image": "spring-onions.jpg", "measures": { "metric": { "amount": 3.0, "unitLong": "", "unitShort": "" }, "us": { "amount": 3.0, "unitLong": "", "unitShort": "" } }, "meta": [ "white", "green", "separated", "chopped" ], "name": "scallions", "original": "3 scallions, chopped, white and green parts separated", "originalName": "scallions, chopped, white and green parts separated", "unit": "" }, { "aisle": "Alcoholic Beverages", "amount": 2.0, "consitency": "liquid", "id": 14106, "image": "white-wine.jpg", "measures": { "metric": { "amount": 2.0, "unitLong": "Tbsps", "unitShort": "Tbsps" }, "us": { "amount": 2.0, "unitLong": "Tbsps", "unitShort": "Tbsps" } }, "meta": [ "white" ], "name": "white wine", "original": "2-3 tbsp white wine", "originalName": "white wine", "unit": "tbsp" }, { "aisle": "Pasta and Rice", "amount": 0.25, "consitency": "solid", "id": 99025, "image": "breadcrumbs.jpg", "measures": { "metric": { "amount": 59.147, "unitLong": "milliliters", "unitShort": "ml" }, "us": { "amount": 0.25, "unitLong": "cups", "unitShort": "cups" } }, "meta": [ "whole wheat", "(I used panko)" ], "name": "whole wheat bread crumbs", "original": "1/4 cup whole wheat bread crumbs (I used panko)", "originalName": "whole wheat bread crumbs (I used panko)", "unit": "cup" } ], "summary": "Pasta with Garlic, Scallions, Cauliflower & Breadcrumbs might be a good recipe to expand your main course repertoire. One portion of this dish contains approximately <b>19g of protein </b>, <b>20g of fat </b>, and a total of <b>584 calories </b>. For <b>\$1.63 per serving </b>, this recipe <b>covers 23% </b> of your daily requirements of vitamins and minerals. This recipe serves 2. It is brought to you by fullbellysisters.blogspot.com. 209 people were glad they tried this recipe. A mixture of scallions, salt and pepper, white wine, and a handful of other ingredients are all it takes to make this recipe so scrumptious. From preparation to the plate, this recipe takes approximately <b>45 minutes </b>. All things considered, we decided this recipe <b>deserves a spoonacular score of 83% </b>. This score is awesome. If you like this recipe, take a look at these similar recipes: <a href=\\"https://spoonacular.com/recipes/cauliflower-gratin-with-garlic-breadcrumbs-318375\\">Cauliflower Gratin with Garlic Breadcrumbs</a>, < href=\\"https://spoonacular.com/recipes/pasta-with-cauliflower-sausage-breadcrumbs-30437\\">Pasta With Cauliflower, Sausage, & Breadcrumbs</a>, and <a href=\\"https://spoonacular.com/recipes/pasta-with-roasted-cauliflower-parsley-and-breadcrumbs-30738\\">Pasta With Roasted Cauliflower, Parsley, And Breadcrumbs</a>.", "winePairing": { "pairedWines": [ "chardonnay", "gruener veltliner", "sauvignon blanc" ], "pairingText": "Chardonnay, Gruener Veltliner, and Sauvignon Blanc are great choices for Pasta. Sauvignon Blanc and Gruner Veltliner both have herby notes that complement salads with enough acid to match tart vinaigrettes, while a Chardonnay can be a good pick for creamy salad dressings. The Buddha Kat Winery Chardonnay with a 4 out of 5 star rating seems like a good match. It costs about 25 dollars per bottle.", "productMatches": [ { "id": 469199, "title": "Buddha Kat Winery Chardonnay", "description": "We barrel ferment our Chardonnay and age it in a mix of Oak and Stainless. Giving this light bodied wine modest oak character, a delicate floral aroma, and a warming finish.", "price": "\$25.0", "imageUrl": "https://spoonacular.com/productImages/469199-312x231.jpg", "averageRating": 0.8, "ratingCount": 1.0, "score": 0.55, "link": "https://www.amazon.com/2015-Buddha-Kat-Winery-Chardonnay/dp/B00OSAVVM4?tag=spoonacular-20" } ] } }';
String json2 = '[ { "name": "", "steps": [ { "equipment": [ { "id": 404784, "image": "oven.jpg", "name": "oven", "temperature": { "number": 200.0, "unit": "Fahrenheit" } } ], "ingredients": [], "number": 1, "step": "Preheat the oven to 200 degrees F." }, { "equipment": [ { "id": 404661, "image": "whisk.png", "name": "whisk" }, { "id": 404783, "image": "bowl.jpg", "name": "bowl" } ], "ingredients": [ { "id": 19334, "image": "light-brown-sugar.jpg", "name": "light brown sugar" }, { "id": 19335, "image": "sugar-in-bowl.png", "name": "granulated sugar" }, { "id": 18371, "image": "white-powder.jpg", "name": "baking powder" }, { "id": 18372, "image": "white-powder.jpg", "name": "baking soda" }, { "id": 12142, "image": "pecans.jpg", "name": "pecans" }, { "id": 20081, "image": "flour.png", "name": "all purpose flour" }, { "id": 2047, "image": "salt.jpg", "name": "salt" } ], "number": 2, "step": "Whisk together the flour, pecans, granulated sugar, light brown sugar, baking powder, baking soda, and salt in a medium bowl." }, { "equipment": [ { "id": 404661, "image": "whisk.png", "name": "whisk" }, { "id": 404783, "image": "bowl.jpg", "name": "bowl" } ], "ingredients": [ { "id": 2050, "image": "vanilla-extract.jpg", "name": "vanilla extract" }, { "id": 93622, "image": "vanilla.jpg", "name": "vanilla bean" }, { "id": 1230, "image": "buttermilk.jpg", "name": "buttermilk" }, { "id": 1123, "image": "egg.png", "name": "egg" } ], "number": 3, "step": "Whisk together the eggs, buttermilk, butter and vanilla extract and vanilla bean in a small bowl." }, { "equipment": [], "ingredients": [ { "id": 1123, "image": "egg.png", "name": "egg" } ], "number": 4, "step": "Add the egg mixture to the dry mixture and gently mix to combine. Do not overmix." }, { "equipment": [], "ingredients": [], "length": { "number": 15, "unit": "minutes" }, "number": 5, "step": "Let the batter sit at room temperature for at least 15 minutes and up to 30 minutes before using." }, { "equipment": [ { "id": 404779, "image": "griddle.jpg", "name": "griddle" }, { "id": 404645, "image": "pan.png", "name": "frying pan" } ], "ingredients": [], "length": { "number": 3, "unit": "minutes" }, "number": 6, "step": "Heat a cast iron or nonstick griddle pan over medium heat and brush with melted butter. Once the butter begins to sizzle, use 2 tablespoons of the batter for each pancake and cook until the bubbles appear on the surface and the bottom is golden brown, about 2 minutes, flip over and cook until the bottom is golden brown, 1 to 2 minutes longer." }, { "equipment": [ { "id": 404784, "image": "oven.jpg", "name": "oven", "temperature": { "number": 200.0, "unit": "Fahrenheit" } } ], "ingredients": [], "number": 7, "step": "Transfer the pancakes to a platter and keep warm in a 200 degree F oven." }, { "equipment": [], "ingredients": [ { "id": 10014037, "image": "bourbon.png", "name": "bourbon" } ], "number": 8, "step": "Serve 6 pancakes per person, top each with some of the bourbon butter." }, { "equipment": [], "ingredients": [ { "id": 19336, "image": "powdered-sugar.jpg", "name": "powdered sugar" }, { "id": 19911, "image": "maple-syrup.png", "name": "maple syrup" } ], "number": 9, "step": "Drizzle with warm maple syrup and dust with confectioners\' sugar." }, { "equipment": [], "ingredients": [ { "id": 12142, "image": "pecans.jpg", "name": "pecans" } ], "number": 10, "step": "Garnish with fresh mint sprigs and more toasted pecans, if desired." } ] }, { "name": "Bourbon Molasses Butter", "steps": [ { "equipment": [ { "id": 404669, "image": "sauce-pan.jpg", "name": "sauce pan" } ], "ingredients": [ { "id": 10014037, "image": "bourbon.png", "name": "bourbon" }, { "id": 19335, "image": "sugar-in-bowl.png", "name": "sugar" } ], "number": 1, "step": "Combine the bourbon and sugar in a small saucepan and cook over high heat until reduced to 3 tablespoons, remove and let cool." }, { "equipment": [ { "id": 404771, "image": "food-processor.png", "name": "food processor" } ], "ingredients": [ { "id": 19304, "image": "molasses.jpg", "name": "molasses" }, { "id": 10014037, "image": "bourbon.png", "name": "bourbon" }, { "id": 2047, "image": "salt.jpg", "name": "salt" } ], "number": 2, "step": "Put the butter, molasses, salt and cooled bourbon mixture in a food processor and process until smooth." }, { "equipment": [ { "id": 404730, "image": "plastic-wrap.jpg", "name": "plastic wrap" }, { "id": 404783, "image": "bowl.jpg", "name": "bowl" } ], "ingredients": [], "number": 3, "step": "Scrape into a bowl, cover with plastic wrap and refrigerate for at least 1 hour to allow the flavors to meld." }, { "equipment": [], "ingredients": [], "length": { "number": 30, "unit": "minutes" }, "number": 4, "step": "Remove from the refrigerator about 30 minutes before using to soften." } ] } ]';
void main() {

  runApp(MyApp());
}


class MyApp extends StatelessWidget {
  

    @override
    Widget build(BuildContext context) {

      sampleRecipeMap = jsonDecode(json1);
      sampleInstructMap = jsonDecode(json2);


      sampleRecipe = new Recipe(new IntSize(20, 20), sampleRecipeMap['title'], sampleRecipeMap['cuisines'][0],  "${sampleRecipeMap['readyInMinutes']} mins", sampleRecipeMap['summary'], sampleRecipeMap['image']);

      String instruction = '';

      sampleInstructMap.forEach((part) {
        instruction += 'For Making  ${part["name"]} :\n';
        part['steps'].forEach((step) {
          instruction += "${step['number']}. ${step['step']} \n";
        });
      });

      sampleRecipe.instruction = instruction;

      return MaterialApp(
        title: 'Welcome to Flutter',
        theme: ThemeData(
          primaryColor: Colors.green,
        ),
        home: RandomWords(),
        // home: Scaffold(
        //   appBar: AppBar(
        //     title: Text('Welcome To Flutter'),
        //   ),
        //   body: Center(
        //     child: RandomWords(),
        //   )
        // )
      );
    }
}


final Uint8List kTransparentImage = new Uint8List.fromList(<int>[
  0x89,
  0x50,
  0x4E,
  0x47,
  0x0D,
  0x0A,
  0x1A,
  0x0A,
  0x00,
  0x00,
  0x00,
  0x0D,
  0x49,
  0x48,
  0x44,
  0x52,
  0x00,
  0x00,
  0x00,
  0x01,
  0x00,
  0x00,
  0x00,
  0x01,
  0x08,
  0x06,
  0x00,
  0x00,
  0x00,
  0x1F,
  0x15,
  0xC4,
  0x89,
  0x00,
  0x00,
  0x00,
  0x0A,
  0x49,
  0x44,
  0x41,
  0x54,
  0x78,
  0x9C,
  0x63,
  0x00,
  0x01,
  0x00,
  0x00,
  0x05,
  0x00,
  0x01,
  0x0D,
  0x0A,
  0x2D,
  0xB4,
  0x00,
  0x00,
  0x00,
  0x00,
  0x49,
  0x45,
  0x4E,
  0x44,
  0xAE,
]);

class IntSize {
  const IntSize(this.width, this.height);

  final int width;
  final int height;
}

class Recipe {
  Recipe(this.intSize, this.name, this.origin, this.time, this.description, this.imgSrc);

  final IntSize intSize;
  final String name;
  final String origin;
  final String time;
  final String description;
  final String imgSrc;
  List<dynamic> ingredients;
  String instruction = '';
}

class _Tile extends StatelessWidget {
  const _Tile(this.index, this.recipe);

  final Recipe recipe;
  final int index;

  @override
  Widget build(BuildContext context) {
    return new Card(
      child: new Column(
        children: <Widget>[
          new Stack(
            children: <Widget>[
              new Center(
                child: new FadeInImage.memoryNetwork(
                  placeholder: kTransparentImage,
                  image: recipe.imgSrc,
                ),
              ),
            ],
          ),
          new Padding(
            padding: const EdgeInsets.all(4.0),
            child: new Row(
              children: <Widget>[
                Expanded(
                  flex: 8,
                  child: new Column(
                    children: <Widget>[
                      new Text(
                        recipe.name,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
                        textAlign: TextAlign.center,
                      ),
                      new Text(
                        recipe.origin,
                        style: const TextStyle(color: Colors.grey),
                      ),
                      new Text(
                        recipe.time,
                        style: const TextStyle(color: Colors.grey),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: new IconButton(
                    icon: Icon(
                      Icons.star_border
                    ), 
                    onPressed: ((){}),
                  ),
                ),
                
              ],
            ),
            
            
          ),
        ],
      ),
    );
  }
}

List<IntSize> _createSizes(int count) {
  Random rnd = new Random();
  return new List.generate(count, (i) => new IntSize((rnd.nextInt(500) + 200), rnd.nextInt(800) + 200));
}

List<dynamic> fetchRandom(int count) {
  return jsonDecode(http.get('https://api.spoonacular.com/recipes/random?number=$count'));
}

List<dynamic> _fetchRecipes(int count) {
  Random rnd = new Random();

  return fetchRandom(count);

  // return new List.generate(count, (i) {
  //   String word = WordPair.random().asPascalCase;
  //   IntSize size = new IntSize((rnd.nextInt(500) + 200), rnd.nextInt(800) + 200);

  //   return new Recipe(size, word, 'France', '10 mins', 'food', 'https://picsum.photos/${size.width}/${size.height}');
  // });
}
class RandomWordsState extends State<RandomWords> {
  final List<WordPair>_suggestions = <WordPair>[];
  final Set<WordPair> _saved = Set<WordPair>();
  final _biggerFont = const TextStyle(fontSize: 18.0);
  final List<dynamic> _recipe = <Recipe>[];
  final Set<Recipe> _savedRecipe = Set<Recipe>();

  

  @override
  // Widget build(BuildContext context) {
  //   return Scaffold(
  //     appBar: AppBar(
  //       title: Text('Startup Name Generator'),
  //       actions: <Widget>[
  //         IconButton(icon: Icon(Icons.list), onPressed: _pushSaved),
  //       ],
  //     ),
  //     body: _buildSuggestions(),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          
          title: Text('Startup Name Generator'),
        ),
        bottomNavigationBar: TabBar(
          labelColor: Colors.green,
          indicatorSize: TabBarIndicatorSize.tab,
          indicatorPadding: EdgeInsets.all(5.0),
          indicatorColor: Colors.green,
          tabs: [
            Tab(icon: Icon(Icons.apps), text: "Recipes",),
            Tab(icon: Icon(Icons.favorite), text: "Selected",),
            Tab(icon: Icon(Icons.assignment), text: "Words",),
          ],
        ),
        body: TabBarView(
          children: [ _pictureGrid(), _savedRecipeGrid(), _buildSuggestions(),],
        ),
      )
    );
  }

  Widget _buildSuggestions() {
    return ListView.builder(
      padding: const EdgeInsets.all(16.0),
      itemBuilder: (context, i) {
        if (i.isOdd) return Divider();

        final index = i ~/ 2;
        if (index >= _suggestions.length) {
          _suggestions.addAll(generateWordPairs().take(10));
        }
        return _buildRow(_suggestions[index]);
      }
    );
  }

  Widget _buildRow(WordPair pair) {
    final bool alreadySaved = _saved.contains(pair);

    return ListTile(
      title: Text(
        pair.asPascalCase,
        style: _biggerFont,
      ),
      // trailing: Icon(
      //   alreadySaved ? Icons.favorite : Icons.favorite_border,
      //   color: alreadySaved ? Colors.purple : null,
      // ),
      trailing: IconButton(
        icon: Icon(
          alreadySaved ? Icons.favorite : Icons.favorite_border,
          color: alreadySaved ? Colors.purple : null,
        ), 
        onPressed: () {
          setState(() {
            if (alreadySaved) {
              _saved.remove(pair);
            } else {
              _saved.add(pair);
            }
          });
        },
      ),
      onTap: () {
        _showDetails(pair);
      }
      
    );
  }

  Widget _savedWords() {
    final Iterable<ListTile> tiles = _saved.map(
      (WordPair pair) {
        return ListTile(
          title: Text(
            pair.asPascalCase,
            style: _biggerFont,
          )
        );
      }
    );
    final List<Widget> divided = ListTile
    .divideTiles(
      context: context,
      tiles: tiles,
    ).toList();

    return Scaffold(
      body: ListView(children: divided),
    );
  }

  Widget _savedRecipeGrid() {
    final Iterable<Widget> tiles = _savedRecipe.map(
      (Recipe recipe) {
        return _tile(recipe);
      }
    ).toList();

    return Scaffold(
      body: OrientationBuilder(
        builder: (context, orientation) {
          int columnCount = orientation == Orientation.portrait ? 2 : 1;

          return new StaggeredGridView.count(
            primary: false,
            crossAxisCount: 4,
            mainAxisSpacing: 4.0,
            crossAxisSpacing: 4.0,
            children: tiles,
            staggeredTiles: List<StaggeredTile>.filled(_savedRecipe.length, StaggeredTile.fit(columnCount)) ,
          );
        },
      ),
    );
  }

  void _pushSaved() {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (BuildContext context) {
          final Iterable<ListTile> tiles = _saved.map(
            (WordPair pair) {
              return ListTile(
                title: Text(
                  pair.asPascalCase,
                  style: _biggerFont,
                )
              );
            }
          );
          final List<Widget> divided = ListTile
          .divideTiles(
            context: context,
            tiles: tiles,
          ).toList();

          return Scaffold(
            appBar: AppBar(
              title: Text('Saved Suggestions'),
            ),
            body: ListView(children: divided),
          );
        }
      )
    );
  }

  void _showDetails(WordPair pair) {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (BuildContext context) {
          return Scaffold(
            appBar: AppBar(
              title: Text('Details for ' + pair.asPascalCase)
            ),
            body: Text(pair.asPascalCase)
          );
        }
      )
    );
  }

  Widget _pictureGrid() {

    return OrientationBuilder(
      builder: (context, orientation) {
        int columnCount = orientation == Orientation.portrait ? 2 : 1;

        return new StaggeredGridView.countBuilder(
          primary: false,
          crossAxisCount: 4,
          mainAxisSpacing: 4.0,
          crossAxisSpacing: 4.0,
          itemBuilder: ((context, index) {
            if (index >= _recipe.length) {
              _recipe.addAll(_fetchRecipes(10));
            }
            return _tile(_recipe[index]);
          }),
          staggeredTileBuilder: (index) => new StaggeredTile.fit(columnCount),
        );
      },
    );


    // return new Scaffold(
    //   appBar: new AppBar(
    //      title: new Text('random dynamic tile sizes'),
    //   ),
    //   body: new StaggeredGridView.countBuilder(
    //     primary: false,
    //     crossAxisCount: 4,
    //     mainAxisSpacing: 4.0,
    //     crossAxisSpacing: 4.0,
    //     itemBuilder: ((context, index) {
    //       return new _Tile(index, _sizes[index]);
    //     }),
    //     staggeredTileBuilder: (index) => new StaggeredTile.fit(2),
    //   ),
    // );
  }

  Widget _tile (recipe) {
    final bool _alreadySaved = _savedRecipe.contains(recipe);

    return InkWell(
      onTap: () => _showRecipeDetails(recipe),
      child: Card(
        child: Column(
          children: <Widget>[
            new Stack(
              children: <Widget>[
                new Center(
                  child: new FadeInImage.memoryNetwork(
                    placeholder: kTransparentImage,
                    image: recipe.imgSrc,
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(4.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 8,
                    child: Column(
                      children: <Widget>[
                        Text(
                          recipe["title"],
                          style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
                          textAlign: TextAlign.center,
                        ),
                        Text(
                          recipe["cuisine"],
                          style: const TextStyle(color: Colors.grey),
                        ),
                        Text(
                          recipe["readyInMinutes"],
                          style: const TextStyle(color: Colors.grey),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: IconButton(
                      icon: Icon(
                        _alreadySaved ? Icons.star : Icons.star_border,
                        color: _alreadySaved ? Colors.yellow : null,
                      ), 
                      onPressed: () {
                        setState(() {
                          if(_alreadySaved) {
                            _savedRecipe.remove(recipe);
                          } else {
                            _savedRecipe.add(recipe);
                          }
                        });
                      },
                    ),
                  ),
                  
                ],
              ),
              
              
            ),
          ],
        ),
      ),
    );
  }

  void _showRecipeDetails(Recipe recipe) {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (BuildContext context) {
          return Scaffold(
            appBar: AppBar(
              title: Text(recipe.name),
              elevation: 0.0,
              backgroundColor: Colors.green.withOpacity(0.8),
              
            ),
            body: ListView(
              children: <Widget>[
                FadeInImage.memoryNetwork(
                  placeholder: kTransparentImage, image: recipe.imgSrc,
                  fit: BoxFit.fill,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Text(recipe.description),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Text('Source: ${recipe.imgSrc}'),
                ), (recipe.instruction != '') ?
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Text('Cooking Instruction: '),
                ) : Text(''), (recipe.instruction != '') ?
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Text(recipe.instruction),
                ): Text(''),
              ],
            ),
            extendBodyBehindAppBar: true,
          );
        }
      )
    );
  }
}

class RandomWords extends StatefulWidget {
  @override
  RandomWordsState createState() {
    return RandomWordsState();
  }
}

